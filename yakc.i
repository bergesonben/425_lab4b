# 1 "yakc.c"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 1 "<command-line>" 2
# 1 "yakc.c"







# 1 "yakk.h" 1







# 1 "yaku.h" 1
# 9 "yakk.h" 2



extern unsigned int YKCtxSwCount;
extern unsigned int YKIdleCount;



enum taskState
{
 RUNNING,
 DELAYED,
 SUSPENDED,
 READY
};

typedef struct taskblock *TCBptr;
typedef struct taskblock
{
    void *stackptr;
  void *taskCode;
    enum taskState state;
    int priority;
    int delay;

    TCBptr next;
    TCBptr prev;
} TCB;

void YKInitialize();
void YKEnterMutex();
void YKExitMutex();
void YKIdleTask();
void YKNewTask(void (* task)(void), void *taskStack, unsigned char priority);
void YKRun(void);
void YKScheduler();
void YKDispatcher();
# 9 "yakc.c" 2







unsigned int YKCtxSwCount;
unsigned int YKIdleCount;
int IdleStk[256];
int kernelStarted = 0;

TCBptr YKRdyList = 0;

TCBptr YKSuspList = 0;
TCBptr YKCurrTask = 0;
TCBptr YKUnusedTCBList;
TCB YKTCBArray[3 +1];


void InsertReadyTask(TCBptr task)
{
  TCBptr tmp;

  if (YKRdyList == 0)
    {
   YKRdyList = task;
   tmp->next = 0;
   tmp->prev = 0;
    }
  tmp = YKRdyList;
  while (tmp->priority < task->priority)
   tmp = tmp->next;

  if (tmp->prev == 0)
   YKRdyList = task;
  else
   tmp->prev->next = task;

  task->prev = tmp->prev;
  task->next = tmp;
}


void YKInitialize()
{
 int i;
  TCBptr tmp, tmp2;

 YKUnusedTCBList = &(YKTCBArray[0]);
    for (i = 0; i < 3; i++)
 {
  YKTCBArray[i].next = &(YKTCBArray[i+1]);
  if(i > 0)
  {
   YKTCBArray[i].prev = &(YKTCBArray[i-1]);
  }
  else
  {
   YKTCBArray[i].prev = 0;
  }
 }
  YKTCBArray[3].next = 0;
 YKNewTask(YKIdleTask, IdleStk, 99);
}

void YKIdleTask()
{
 while(1)
 {
  YKEnterMutex();
  ++YKIdleCount;
  YKExitMutex();
 }
}

void YKNewTask(void (* task)(void), void *taskStack, unsigned char priority)
{



 TCBptr tmp;
 unsigned * tempStack;
 int i;
 if(YKUnusedTCBList == 0)
  return;
 tmp = YKUnusedTCBList;
 YKUnusedTCBList = tmp->next;
 YKUnusedTCBList->prev = 0;
 tmp->priority = priority;
 tmp->state = READY;
 tmp->delay = 0;
 tmp->stackptr = (unsigned *)taskStack;
 tmp->taskCode = task;

 tempStack = (unsigned *)taskStack;
 tempStack -= 12;

 for(i = 0; i < 12; ++i)
 {
  tempStack[i] = 0;
 }
 tempStack[9] = task;
 tempStack[11] = 0;

 InsertReadyTask(tmp);
 if(kernelStarted)
 {
  YKScheduler();
 }
}

void YKRun(void)
{
 TCBptr tmp;

 if (kernelStarted)
  return;

 YKEnterMutex();
 kernelStarted = 1;
 YKExitMutex();

 YKScheduler();
}

void YKScheduler()
{
 if (YKCurrTask == 0)
 {
  YKCurrTask = YKRdyList;
  YKRdyList = YKCurrTask->next;
  YKRdyList->prev = 0;
  YKDispatcher();
  return;
 }

 if (YKCurrTask->priority < YKRdyList->priority)
  return;

 InsertReadyTask(YKCurrTask);


 SaveContext();
 YKCurrTask = YKRdyList;
 YKRdyList = YKRdyList->next;
 YKRdyList->prev = 0;

 YKDispatcher();
}
