//Kernel routines written in C. Global variables used by kernel code or shared by kernel and application code should also be defined in this file.

// How do we push everything in the dispatcher
// How do we access the values being popped off by iret
// Should saving context happen in the scheduler?
// Should the scheduler be in assembly?

#include "yakk.h"// TODO: save context for YKCurrTask
	// IP, CS, Flags, Regs

#define DEFAULT_STACK_SIZE 12
#define TASK_INDEX 9
#define FLAGS_INDEX 11
#define FLAGS_MASK 0 //TODO: fix

unsigned int YKCtxSwCount;
unsigned int YKIdleCount;
int IdleStk[STACK_SIZE];
int kernelStarted = 0;

TCBptr YKRdyList = NULL;				/* a list of TCBs of all ready tasks
				   												in order of decreasing priority */
TCBptr YKSuspList = NULL;				/* tasks delayed or suspended */
TCBptr YKCurrTask = NULL;				/* the current task running*/
TCBptr YKUnusedTCBList;					/* a list of available TCBs */
TCB    YKTCBArray[MAX_TASKS+1];	/* array to allocate all needed TCBs
				   												(extra one is for the idle task) */

void InsertReadyTask(TCBptr task)
{
		TCBptr tmp;

		if (YKRdyList == NULL)	/* is this first insertion? */
    {
			YKRdyList = task;
			tmp->next = NULL;
			tmp->prev = NULL;
    }
		tmp = YKRdyList;
		while (tmp->priority < task->priority)
			tmp = tmp->next;

		if (tmp->prev == NULL)
			YKRdyList = task;
		else
			tmp->prev->next = task;

		task->prev = tmp->prev;
		task->next = tmp;
}


void YKInitialize()
{
	int i;
  TCBptr tmp, tmp2;
	// code to construct doubly linked available TCB list from initial
	YKUnusedTCBList = &(YKTCBArray[0]);
    for (i = 0; i < MAX_TASKS; i++)
	{
		YKTCBArray[i].next = &(YKTCBArray[i+1]);
		if(i > 0)
		{
			YKTCBArray[i].prev = &(YKTCBArray[i-1]);
		}
		else
		{
			YKTCBArray[i].prev = NULL;
		}
	}
  YKTCBArray[MAX_TASKS].next = NULL;
	YKNewTask(YKIdleTask, IdleStk, 99);
}

void YKIdleTask()
{
	while(1)
	{
		YKEnterMutex();
		++YKIdleCount;
		YKExitMutex();
	}
}

void YKNewTask(void (* task)(void), void *taskStack, unsigned char priority)
{
	/* code to insert an entry in doubly linked ready list sorted by
       priority numbers (lowest number first).  tmp points to TCB
       to be inserted */
	TCBptr tmp;
	unsigned * tempStack;
	int i;
	if(YKUnusedTCBList == NULL)
		return;
	tmp = YKUnusedTCBList;
	YKUnusedTCBList = tmp->next;
	YKUnusedTCBList->prev = NULL;
	tmp->priority = priority;
	tmp->state = READY;
	tmp->delay = 0;
	tmp->stackptr = (unsigned *)taskStack;
	tmp->taskCode = task;

	tempStack = (unsigned *)taskStack;
	tempStack -= DEFAULT_STACK_SIZE;

	for(i = 0; i < 12; ++i)
	{
		tempStack[i] = 0;
	}
	tempStack[TASK_INDEX]  = task;
	tempStack[FLAGS_INDEX] = FLAGS_MASK;

	InsertReadyTask(tmp);
	if(kernelStarted)
	{
		YKScheduler();
	}
}

void YKRun(void)
{
	TCBptr tmp;

	if (kernelStarted)
		return;

	YKEnterMutex();
	kernelStarted = 1;
	YKExitMutex();

	YKScheduler();
}

void YKScheduler()
{
	if (YKCurrTask == NULL)
	{
		YKCurrTask = YKRdyList;
		YKRdyList = YKCurrTask->next;
		YKRdyList->prev = NULL;
		YKDispatcher();
		return;
	}

	if (YKCurrTask->priority < YKRdyList->priority)
		return;

	InsertReadyTask(YKCurrTask);
	// TODO: save context for YKCurrTask
	// IP, CS, Flags, Regs
	SaveContext();
	YKCurrTask = YKRdyList;
	YKRdyList = YKRdyList->next;
	YKRdyList->prev = NULL;

	YKDispatcher();
}

/*
void YKDelayTask(unsigned count)
{
}

void YKEnterISR()
{
}

void YKExitISR()
{
}

void YKTickHandler()
{
}

YKSEM* YKSemCreate(int initialValue)
{
	return 0;
}

void YKSemPend(YKSEM *semaphore)
{
}

void YKSemPost(YKSEM *semaphore)
{
}

YKQ *YKQCreate(void **start, unsigned size)
{
	return 0;
}

void *YKQPend(YKQ *queue)
{
}

int YKQPost(YKQ *queue, void *msg)
{
}

YKEVENT *YKEventCreate(unsigned initialValue)
{
	return 0;
}


unsigned YKEventPend(YKEVENT *event, unsigned eventMask, int waitMode)
{
	return 0;
}

void YKEventSet(YKEVENT *event, unsigned eventMask)
{
}

void YKEventReset(YKEVENT *event, unsigned eventMask)
{
}
*/
