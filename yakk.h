//A .h file for kernel code, not modified by user.
//It should include declarations such as the TCB, YKSEM, and YKQ, as well as prototypes for the kernel functions.
//Global variables shared by kernel and application code should be declared as extern in this file.

//it should include extern declarations for things defined in yakc.c, such as YKCtxSwCount and YKIdleCount, as well as the prototypes for the kernel functions. This file should not include any code or definitions that the application programmer might need to modify.
#ifndef YAKK__H_
#define YAKK__H_
#include "yaku.h"

#define NULL 0

extern unsigned int YKCtxSwCount;
extern unsigned int YKIdleCount;

//extern unsigned int YKTickNum;

enum taskState
{
	RUNNING,
	DELAYED,
	SUSPENDED,
	READY
};

typedef struct taskblock *TCBptr;
typedef struct taskblock
{							/* the TCB struct definition */
    void *stackptr;			/* pointer to current top of stack */
		void *taskCode;
    enum taskState state;		/* current state */
    int priority;			/* current priority */
    int delay;				/* #ticks yet to wait */

    TCBptr next;			/* forward ptr for dbl linked list */
    TCBptr prev;			/* backward ptr for dbl linked list */
}  TCB;

void YKInitialize();
void YKEnterMutex();
void YKExitMutex();
void YKIdleTask();
void YKNewTask(void (* task)(void), void *taskStack, unsigned char priority);
void YKRun(void);
void YKScheduler();
void YKDispatcher();

/*
void YKDelayTask(unsigned count);
void YKEnterISR();
void YKExitISR();
void YKTickHandler();
YKSEM* YKSemCreate(int initialValue);
void YKSemPend(YKSEM *semaphore);
void YKSemPost(YKSEM *semaphore);
YKQ *YKQCreate(void **start, unsigned size);
void *YKQPend(YKQ *queue);
int YKQPost(YKQ *queue, void *msg);
YKEVENT *YKEventCreate(unsigned initialValue);
unsigned YKEventPend(YKEVENT *event, unsigned eventMask, int waitMode);
void YKEventSet(YKEVENT *event, unsigned eventMask);
void YKEventReset(YKEVENT *event, unsigned eventMask);
*/

#endif /* YAKK__H_ */
