;Kernel routines written in assembly

YKEnterMutex:
	cli
	ret

YKExitMutex:
	sti
	ret

YKDispatcher:
  call	YKEnterMutex
  inc		word[YKCtxSwCount]
  call  YKExitMutex
  mov 	sp, [YKCurrTask]

  pop		ds
	pop 	es
	pop 	di
	pop 	si
	pop 	bp
  pop 	dx
	pop 	cx
  pop 	bx
  pop 	ax
  iret

SaveContext:
  int 0 
	sti

  push ax
  push bx
  push cx
  push dx
  push bp
  push si
  push di
  push es
  push ds
	ret
