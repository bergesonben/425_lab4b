#####################################################################
# ECEn 425 Lab 4B Makefile

lab4B.bin:	lab4Bfinal.s
		nasm lab4Bfinal.s -o lab4B.bin -l lab4B.lst

lab4Bfinal.s:	clib.s myisr.s myinth.s lab4b_app.s
		cat clib.s myisr.s myinth.s lab4b_app.s > lab4Bfinal.s

myinth.s:	myinth.c
		cpp myinth.c myinth.i
		c86 -g myinth.i myinth.s

lab4b_app.s:	lab4b_app.c
		cpp lab4b_app.c lab4b_app.i
		c86 -g lab4b_app.i lab4b_app.s

clean:
		rm lab4B.bin lab4B.lst lab4Bfinal.s myinth.s myinth.i \
		lab4b_app.s lab4b_app.i
